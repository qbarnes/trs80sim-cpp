#ifndef KBD_class_h
#define KBD_class_h

#include "Z80.h"
#include "BUS.h"
#include "keys.h"
#include "HOSTclass.h"
#include "ROMclass.h"

class KBD : public ROM {

	void ReadUpdate(void);

protected:
	virtual MEM_8bit Value(address);

public:
	KBD(unsigned int size) : ROM(size) {}

};  // class KBD

#endif
