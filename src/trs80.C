#include "TRSclass.h"
#include "HOSTclass.h"

Host	*host;

int
main(int argc, char *argv[])
{
	TRS_80	*trs80;

	host  = new Host("TRS-80", argc, argv);
	trs80 = new TRS_80;

	return( trs80->run() );

}
