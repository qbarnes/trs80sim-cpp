#ifndef Z80_cpu_h
#define Z80_cpu_h

#if defined(DEBUG) || defined(TRACE)
#include <iostream>
#include <fstream>
#include <stdlib>
#endif

#include "Z80.h"
#include "Z80_alu8.h"
#include "Z80_alu16.h"
#include "Z80_reg.h"
#include "Z80_manip.h"

#include "MemBUS.h"
#include "IO_BUS.h"

class z80_cpu {

	enum	IntrState {
		Disabled,
		Enabled,
	};

	Bit		Halt;
	IntrState	IFF1, IFF2;

	class MemBUS& z80mem;
	class IO_BUS& z80io;

	z80_registers	reg;
	z80_flags	flag;
	z80_alu8	alu8;
	z80_alu16	alu16;

#ifdef TRACE
	ofstream	trace;
#endif
	static const char tab, comma, eol, lparen, rparen;

#ifdef TRACE
	inline	ostream& show_ind(ostream&, reg16bit, z80_8bit_s = 0);
	inline	ostream& show_ind(ostream&, address);
#endif

	inline void intr_check();
	inline void intr_state(IntrState);

	void emulator_cb_inst(void);
	void emulator_dd_inst(void);
	void emulator_ed_inst(void);
	void emulator_fd_inst(void);

	inline z80_8bit pc_fetch(void);
	inline z80_16bit pc_fetch_nn(void);

	inline void ex_reg16_mem(reg16bit, address);
	inline void ex_regs16(reg16bit, reg16bit);

	inline void ld(reg8bit, reg8bit);
	inline void ld(reg8bit, z80_8bit);
	inline void ld_ind(reg8bit, reg16bit, z80_8bit_s = 0);
	inline void ld_ind(reg8bit, address);

	inline void ld(reg16bit, reg16bit);
	inline void ld(reg16bit, z80_16bit);
	inline void ld_ind(reg16bit, reg16bit);
	inline void ld_ind(reg16bit, address);

	inline void ld_mem(reg16bit, reg8bit);
	inline void ld_mem(reg16bit, z80_8bit);
	inline void ld_mem(reg16bit, z80_8bit_s, reg8bit);
	inline void ld_mem(reg16bit, z80_8bit_s, z80_8bit);
	inline void ld_mem(reg16bit, reg16bit);
	inline void ld_mem(address, reg8bit);
	inline void ld_mem(address, reg16bit);

	inline void ex_mem(reg16bit, reg16bit);

	inline void ldi_mc();
	inline void ldi();
	inline void ldir();
	inline void ldd_mc();
	inline void ldd();
	inline void lddr();

	inline void cpi_mc();
	inline void cpi();
	inline void cpir();
	inline void cpd_mc();
	inline void cpd();
	inline void cpdr();

	inline void ina(z80_8bit);
	inline void in(reg8bit);
	inline void in_cp(reg8bit);

	inline void ini_mc();
	inline void ini();
	inline void inir();
	inline void ind_mc();
	inline void ind();
	inline void indr();

	inline void outa(z80_8bit);
	inline void out(reg8bit);

	inline void oti_mc();
	inline void outi();
	inline void otir();
	inline void otd_mc();
	inline void outd();
	inline void otdr();

	inline void rlca();
	inline void rrca();
	inline void rla();
	inline void rra();

	inline void rlc(reg8bit);
	inline void rlc_ind(reg16bit, z80_8bit_s = 0);
	inline void rl(reg8bit);
	inline void rl_ind(reg16bit, z80_8bit_s = 0);
	inline void rrc(reg8bit);
	inline void rrc_ind(reg16bit, z80_8bit_s = 0);
	inline void rr(reg8bit);
	inline void rr_ind(reg16bit, z80_8bit_s = 0);
	inline void sla(reg8bit);
	inline void sla_ind(reg16bit, z80_8bit_s = 0);
	inline void sra(reg8bit);
	inline void sra_ind(reg16bit, z80_8bit_s = 0);
	inline void srl(reg8bit);
	inline void srl_ind(reg16bit, z80_8bit_s = 0);

	inline void rld();
	inline void rrd();

	inline void bit(z80_8bit, reg8bit);
	inline void bit(z80_8bit, z80_8bit);
	inline void bit_ind(z80_8bit, reg16bit, z80_8bit_s = 0);
	inline void set(z80_8bit, reg8bit);
	inline void set_ind(z80_8bit, reg16bit, z80_8bit_s = 0);
	inline void res(z80_8bit, reg8bit);
	inline void res_ind(z80_8bit, reg16bit, z80_8bit_s = 0);

	inline void _push(z80_16bit);
	inline void _push(reg16bit);

	inline void push(z80_16bit);
	inline void push(reg16bit);

	inline z80_16bit pop(void);
	inline void pop(reg16bit);

	inline void inc(reg8bit);
	inline void inc(reg16bit);
	inline void inc_ind(reg16bit, z80_8bit_s = 0);

	inline void dec(reg8bit);
	inline void dec(reg16bit);
	inline void dec_ind(reg16bit, z80_8bit_s = 0);

	inline void add_acc(reg8bit);
	inline void add_acc(z80_8bit);
	inline void add_acc_ind(reg16bit, z80_8bit_s = 0);

	inline void sub_acc(reg8bit);
	inline void sub_acc(z80_8bit);
	inline void sub_acc_ind(reg16bit, z80_8bit_s = 0);

	inline void adc_acc(reg8bit);
	inline void adc_acc(z80_8bit);
	inline void adc_acc_ind(reg16bit, z80_8bit_s = 0);

	inline void sbc_acc(reg8bit);
	inline void sbc_acc(z80_8bit);
	inline void sbc_acc_ind(reg16bit, z80_8bit_s = 0);

	inline void cp_acc(reg8bit);
	inline void cp_acc(z80_8bit);
	inline void cp_acc_ind(reg16bit, z80_8bit_s = 0);

	inline void neg_acc();
	inline void cpl_acc();

	inline void scf();
	inline void ccf();

	inline void and_acc(reg8bit);
	inline void and_acc(z80_8bit);
	inline void and_acc_ind(reg16bit, z80_8bit_s = 0);

	inline void or_acc(reg8bit);
	inline void or_acc(z80_8bit);
	inline void or_acc_ind(reg16bit, z80_8bit_s = 0);

	inline void xor_acc(reg8bit);
	inline void xor_acc(z80_8bit);
	inline void xor_acc_ind(reg16bit, z80_8bit_s = 0);

	inline void daa();

	inline void add_HL(reg16bit);
	inline void add_IX(reg16bit);
	inline void add_IY(reg16bit);

	inline void adc_HL(reg16bit);
	inline void sbc_HL(reg16bit);

	inline void call(z80_16bit);
	inline void call(Flags, z80_16bit);
	inline void call(NotFlags, z80_16bit);

	inline void ret();
	inline void ret(Flags);
	inline void ret(NotFlags);
	inline void retn();
	inline void reti();

	inline void jp(z80_16bit);
	inline void jp(Flags, z80_16bit);
	inline void jp(NotFlags, z80_16bit);
	inline void jp(reg16bit);

	inline void jr(z80_8bit_s);
	inline void jr(Flags, z80_8bit_s);
	inline void jr(NotFlags, z80_8bit_s);

	inline void djnz(reg8bit, z80_8bit_s);

	inline void rst(z80_16bit);
	inline void halt();

	inline void ex(reg16bit,reg16bit);
	inline void exx();

	inline void di();
	inline void ei();

	inline void im(int);

public:
	z80_cpu(MemBUS&, IO_BUS&);
#ifdef TRACE
	z80_cpu(MemBUS&, IO_BUS&, const char *);
#endif

	int emulator(unsigned int = 0);
	void mem_dump(unsigned int, unsigned int);
	void mem_write(unsigned int, const void *, unsigned int);
	void mem_read(unsigned int, void *, unsigned int);
};

#ifdef TRACE
inline	ostream&
z80_cpu::show_ind(ostream& str, reg16bit ri, z80_8bit_s dis)
{
	str << lparen << reg.name(ri);

	if ( dis != 0 ) {
		str << showsign(dis);
	}

	return( str << rparen );

}

inline	ostream&
z80_cpu::show_ind(ostream& str, address a)
{
	return( str << lparen << int(a) << rparen );
}
#endif

inline void
z80_cpu::intr_check()
{
	reg.r8(R)++;

	if ( IFF1 == Enabled ) {
		// Do interrupt check here.
	}
}

inline void
z80_cpu::intr_state(IntrState state)
{
	IFF1 = state;
}

inline z80_8bit
z80_cpu::pc_fetch(void)
{
#ifndef DEBUG
	return( z80mem[reg.r16(PC)++] );
#else
	unsigned int pc = reg.r16(PC)++;
	z80_8bit mem = z80mem[ pc ];
	cout << "pc = 0x" << hexword((int)pc) <<
		" 0x" << hexbytep((int)mem) << endl;
	return( mem );
#endif
}

inline z80_16bit
z80_cpu::pc_fetch_nn(void)
{
	z80_16bit nn;

	nn  = (z80_16bit)pc_fetch();
	nn |= (z80_16bit)(pc_fetch()) << 8;

	return( nn );
}

inline void 
z80_cpu::ex_reg16_mem(reg16bit r, address a)
{
	z80_8bit	v;

	v = z80mem[a];
	z80mem[a] = reg.lsb(r);
	reg.lsb(r) = v;

	v = z80mem[++a];
	z80mem[a] = reg.msb(r);
	reg.msb(r) = v;
}

inline void
z80_cpu::ex_regs16(reg16bit r1, reg16bit r2)
{
	z80_16bit t = reg.r16(r1);
	reg.r16(r1) = reg.r16(r2);
	reg.r16(r2) = t;
}

inline void
z80_cpu::rlca()
{
#ifdef TRACE
	trace << "RLCA";
#endif
	reg.r8(A) = alu8.rlca(reg.r8(A));
}

inline void
z80_cpu::rrca()
{
#ifdef TRACE
	trace << "RRCA";
#endif
	reg.r8(A) = alu8.rrca(reg.r8(A));
}

inline void
z80_cpu::rla()
{
#ifdef TRACE
	trace << "RLA";
#endif
	reg.r8(A) = alu8.rla(reg.r8(A));
}

inline void
z80_cpu::rra()
{
#ifdef TRACE
	trace << "RRA";
#endif
	reg.r8(A) = alu8.rra(reg.r8(A));
}

inline void
z80_cpu::rlc(reg8bit r)
{
#ifdef TRACE
	trace << "RLC" << tab << reg.name(r);
#endif
	reg.r8(r) = alu8.rlc(reg.r8(r));
}

inline void
z80_cpu::rlc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "RLC" << tab;
	show_ind(trace, ri, dis);
#endif
	z80_16bit loc = reg.r16(ri) + dis;
	z80mem[loc] = alu8.rlc(z80mem[loc]);
}

inline void
z80_cpu::rl(reg8bit r)
{
#ifdef TRACE
	trace << "RL" << tab << reg.name(r);
#endif
	reg.r8(r) = alu8.rl(reg.r8(r));
}

inline void
z80_cpu::rl_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "RL" << tab;
	show_ind(trace, ri, dis);
#endif
	z80_16bit loc = reg.r16(ri) + dis;
	z80mem[loc] = alu8.rl(z80mem[loc]);
}

inline void
z80_cpu::rrc(reg8bit r)
{
#ifdef TRACE
	trace << "RRC" << tab << reg.name(r);
#endif
	reg.r8(r) = alu8.rrc(reg.r8(r));
}

inline void
z80_cpu::rrc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "RRC" << tab;
	show_ind(trace, ri, dis);
#endif
	z80_16bit loc = reg.r16(ri) + dis;
	z80mem[loc] = alu8.rrc(z80mem[loc]);
	// MEM_Ref&	m = z80mem[reg.r16(ri) + dis];
	// m = alu8.rr(m);
}

inline void
z80_cpu::rr(reg8bit r)
{
#ifdef TRACE
	trace << "RR" << tab << reg.name(r);
#endif
	reg.r8(r) = alu8.rr(reg.r8(r));
}

inline void
z80_cpu::rr_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "RR" << tab;
	show_ind(trace, ri, dis);
#endif
	z80_16bit loc = reg.r16(ri) + dis;
	z80mem[loc] = alu8.rr(z80mem[loc]);
}

inline void
z80_cpu::sla(reg8bit r)
{
#ifdef TRACE
	trace << "SLA" << tab << reg.name(r);
#endif
	reg.r8(r) = alu8.sla(reg.r8(r));
}

inline void
z80_cpu::sla_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "SLA" << tab;
	show_ind(trace, ri, dis);
#endif
	z80_16bit loc = reg.r16(ri) + dis;
	z80mem[loc] = alu8.sla(z80mem[loc]);
}

inline void
z80_cpu::sra(reg8bit r)
{
#ifdef TRACE
	trace << "SRA" << tab << reg.name(r);
#endif
	reg.r8(r) = alu8.sra(reg.r8(r));
}

inline void
z80_cpu::sra_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "SRA" << tab;
	show_ind(trace, ri, dis);
#endif
	z80_16bit loc = reg.r16(ri) + dis;
	z80mem[loc] = alu8.sra(z80mem[loc]);
}

inline void
z80_cpu::srl(reg8bit r)
{
#ifdef TRACE
	trace << "SRL" << tab << reg.name(r);
#endif
	reg.r8(r) = alu8.srl(reg.r8(r));
}

inline void
z80_cpu::srl_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "SRL" << tab;
	show_ind(trace, ri, dis);
#endif
	z80_16bit loc = reg.r16(ri) + dis;
	z80mem[loc] = alu8.srl(z80mem[loc]);
}

inline void
z80_cpu::rld()
{
#ifdef TRACE
	trace << "RLD";
#endif
	reg8bit_pair_t	rp;
	z80_16bit loc = reg.r16(HL);

	rp = alu8.rld(reg.r8(A),z80mem[loc]);
	reg.r8(A)   = rp.r1;
	z80mem[loc] = rp.r2;
}

inline void
z80_cpu::rrd()
{
#ifdef TRACE
	trace << "RRD";
#endif
	reg8bit_pair_t	rp;
	z80_16bit loc = reg.r16(HL);

	rp = alu8.rrd(reg.r8(A),z80mem[loc]);
	reg.r8(A)   = rp.r1;
	z80mem[loc] = rp.r2;
}

inline void
z80_cpu::bit(z80_8bit s, reg8bit r)
{
#ifdef TRACE
	trace << "BIT" << tab << (int)s << comma << reg.name(r);
#endif
	alu8.bit(s,reg.r8(r));
}

inline void
z80_cpu::bit(z80_8bit s, z80_8bit v)
{
#ifdef TRACE
	trace << "BIT" << tab << (int)s << comma << (int)v;
#endif
	alu8.bit(s,v);
}

inline void
z80_cpu::bit_ind(z80_8bit s, reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "BIT" << tab << (int)s << comma;
	show_ind(trace, ri, dis);
#endif
	alu8.bit(s,z80mem[reg.r16(ri) + dis]);
}

inline void
z80_cpu::set(z80_8bit s, reg8bit r)
{
#ifdef TRACE
	trace << "SET" << tab << (int)s << comma << reg.name(r);
#endif
	reg.r8(r) = alu8.set(s,reg.r8(r));
}

inline void
z80_cpu::set_ind(z80_8bit s, reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "SET" << tab << (int)s << comma;
	show_ind(trace, ri, dis);
#endif
	z80_16bit loc = reg.r16(ri) + dis;
	z80mem[loc] = alu8.set(s,z80mem[loc]);
}

inline void
z80_cpu::res(z80_8bit s, reg8bit r)
{
#ifdef TRACE
	trace << "RES" << tab << (int)s << comma << reg.name(r);
#endif
	reg.r8(r) = alu8.res(s,reg.r8(r));
}

inline void
z80_cpu::res_ind(z80_8bit s, reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "RES" << tab << (int)s << comma;
	show_ind(trace, ri, dis);
#endif
	z80_16bit loc = reg.r16(ri) + dis;
	z80mem[loc] = alu8.res(s,z80mem[loc]);
}

inline void
z80_cpu::_push(z80_16bit v)
{
	z80mem[--reg.r16(SP)] = (z80_8bit)(v >> bpb);
	z80mem[--reg.r16(SP)] = (z80_8bit)v;
}

inline void
z80_cpu::_push(reg16bit r)
{
	z80mem[--reg.r16(SP)] = reg.msb(r);
	z80mem[--reg.r16(SP)] = reg.lsb(r);
}

inline void
z80_cpu::push(z80_16bit v)
{
#ifdef TRACE
	trace << "PUSH" << tab << hexwordb((int)v);
#endif
	_push(v);
}

inline void
z80_cpu::push(reg16bit r)
{
#ifdef TRACE
	trace << "PUSH" << tab << reg.name(r);
#endif
	_push(r);
}

inline z80_16bit
z80_cpu::pop(void)
{
	z80_16bit v;

	v  = z80_8bit(z80mem[reg.r16(SP)++]);
	v |= z80_8bit(z80mem[reg.r16(SP)++]) << bpb;

#ifdef TRACE
	trace << "pop" << tab << hexwordb((int)v);
#endif
	return(v);
}

inline void
z80_cpu::pop(reg16bit r)
{
#ifdef TRACE
	trace << "POP" << tab << reg.name(r);
#endif
	reg.lsb(r) = z80mem[reg.r16(SP)++];
	reg.msb(r) = z80mem[reg.r16(SP)++];
}

inline void
z80_cpu::ld(reg8bit rd, reg8bit rs)
{
#ifdef TRACE
	trace << "LD" << tab << reg.name(rd) << comma << reg.name(rs);
#endif
	reg.r8(rd) = reg.r8(rs);
}

inline void
z80_cpu::ld(reg8bit rd, z80_8bit n)
{
#ifdef TRACE
	trace << "LD" << tab << reg.name(rd)
		<< comma << hexbyte((int)n);
#endif
	reg.r8(rd) = n;
}

inline void
z80_cpu::ld_ind(reg8bit rd, reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "LD" << tab << reg.name(rd) << comma;
	show_ind(trace, ri, dis);
#endif
	reg.r8(rd) = z80mem[reg.r16(ri) + dis];
}

inline void
z80_cpu::ld_ind(reg8bit rd, address addr)
{
#ifdef TRACE
	trace << "LD" << tab << reg.name(rd) << comma
		<< hexwordp((int)addr);
#endif
	reg.r8(rd) = z80mem[addr];
}

inline void
z80_cpu::ld(reg16bit rd, reg16bit rs)
{
#ifdef TRACE
	trace << "LD" << tab << reg.name(rd) << comma << reg.name(rs);
#endif
	reg.r16(rd) = reg.r16(rs);
}

inline void
z80_cpu::ld(reg16bit rd, z80_16bit n)
{
#ifdef TRACE
	trace << "LD" << tab << reg.name(rd)
		<< comma << hexword((int)n);
#endif
	reg.r16(rd) = n;
}

inline void
z80_cpu::ld_ind(reg16bit rd, reg16bit ri)
{
#ifdef TRACE
	trace << "LD" << tab << reg.name(rd) << comma;
	show_ind(trace, ri);
#endif
	z80_16bit addr = reg.r16(ri);

	reg.lsb(rd) = z80mem[ addr++ ];
	reg.msb(rd) = z80mem[ addr   ];
}

inline void
z80_cpu::ld_ind(reg16bit rd, address addr)
{
#ifdef TRACE
	trace << "LD" << tab << reg.name(rd) << comma
		<< hexwordp((int)addr);
#endif
	reg.lsb(rd) = z80mem[ addr++ ];
	reg.msb(rd) = z80mem[ addr   ];
}

// 	LD	(RR),R
inline void
z80_cpu::ld_mem(reg16bit ri, reg8bit rs)
{
#ifdef TRACE
	trace << "LD" << tab;
	show_ind(trace, ri) << comma << reg.name(rs);
#endif
	z80mem[ reg.r16(ri) ] = reg.r8(rs);
}

// 	LD	(RR),N
inline void
z80_cpu::ld_mem(reg16bit ri, z80_8bit n)
{
#ifdef TRACE
	trace << "LD" << tab;
	show_ind(trace, ri) << comma << int(n);
#endif
	z80mem[ reg.r16(ri) ] = n;
}

//	LD	(RR+IND),R
inline void
z80_cpu::ld_mem(reg16bit ri,z80_8bit_s dis, reg8bit rs)
{
#ifdef TRACE
	trace << "LD" << tab;
	show_ind(trace, ri, dis) << comma << reg.name(rs);
#endif
	z80mem[ reg.r16(ri) + dis ] = reg.r8(rs);
}

//	LD	(RR+IND),N
inline void
z80_cpu::ld_mem(reg16bit ri,z80_8bit_s dis, z80_8bit n)
{
#ifdef TRACE
	trace << "LD" << tab;
	show_ind(trace, ri, dis) << comma << int(n);
#endif
	z80mem[ reg.r16(ri) + dis ] = n;
}

//	LD	(RR),RR
inline void
z80_cpu::ld_mem(reg16bit ri, reg16bit rs)
{
#ifdef TRACE
	trace << "LD" << tab;
	show_ind(trace, ri) << comma << reg.name(rs);
#endif
	z80_16bit addr = reg.r16(ri);

	z80mem[ addr++ ] = reg.lsb(rs);
	z80mem[ addr   ] = reg.msb(rs);
}

//	LD	(NN),R
inline void
z80_cpu::ld_mem(address addr, reg8bit rs)
{
#ifdef TRACE
	trace << "LD" << tab;
	show_ind(trace, addr) << comma << reg.name(rs);
#endif
	z80mem[ addr ] = reg.r8(rs);
}

//	LD	(NN),RR
inline void
z80_cpu::ld_mem(address addr, reg16bit rs)
{
#ifdef TRACE
	trace << "LD" << tab;
	show_ind(trace, addr) << comma << reg.name(rs);
#endif
	z80mem[ addr++ ] = reg.lsb(rs);
	z80mem[ addr   ] = reg.msb(rs);
}

//	EX	(RR),RR
inline void
z80_cpu::ex_mem(reg16bit ri, reg16bit rs)
{
#ifdef TRACE
	trace << "EX" << tab;
	show_ind(trace, ri) << comma << reg.name(rs);
#endif
	ex_reg16_mem( rs, reg.r16(ri) );
}

inline void
z80_cpu::ldi_mc()
{
	// why is cast needed?
	// non-static reference member `MemBUS&MEM_Ref::mem', can't use 
	// default assignment operator
	z80mem[reg.r16(DE)++] = (int)z80mem[reg.r16(HL)++];
	flag[Subtract] = 0;
	flag[HalfCarry] = 0;
	flag[ParOvr] = ( --reg.r16(BC) != 0 );
}

inline void
z80_cpu::ldi()
{
#ifdef TRACE
	trace << "LDI";
#endif
	ldi_mc();
	intr_check();
}

inline void
z80_cpu::ldir()
{
#ifdef TRACE
	trace << "LDIR";
#endif
	do {

		ldi_mc();
		intr_check();

	} while ( flag[ParOvr] );
}

inline void
z80_cpu::ldd_mc()
{
	// Another mystery cast.
	z80mem[reg.r16(DE)--] = (int)z80mem[reg.r16(HL)--];
	flag[Subtract] = 0;
	flag[HalfCarry] = 0;
	flag[ParOvr] = ( --reg.r16(BC) != 0 );
}

inline void
z80_cpu::ldd()
{
#ifdef TRACE
	trace << "LDD";
#endif
	ldd_mc();
}

inline void
z80_cpu::lddr()
{
#ifdef TRACE
	trace << "LDDR";
#endif
	do {

		ldd_mc();
		intr_check();

	} while ( flag[ParOvr] );
}


inline void
z80_cpu::cpi_mc()
{
	alu8.cp(reg.r8(A), z80mem[reg.r16(HL)++]);
	flag[ParOvr] = ( --reg.r16(BC) != 0 );
}

inline void
z80_cpu::cpi()
{
#ifdef TRACE
	trace << "CPI";
#endif
	cpi_mc();
}

inline void
z80_cpu::cpir()
{
#ifdef TRACE
	trace << "CPIR";
#endif
	do {

		cpi_mc();
		intr_check();

	} while ( flag[NotZero] && flag[ParOvr] );
}

inline void
z80_cpu::cpd_mc()
{
	alu8.cp(reg.r8(A), z80mem[reg.r16(HL)--]);
	flag[ParOvr] = ( --reg.r16(BC) != 0 );
}

inline void
z80_cpu::cpd()
{
#ifdef TRACE
	trace << "CPD";
#endif
	cpd_mc();
}

inline void
z80_cpu::cpdr()
{
#ifdef TRACE
	trace << "CPDR";
#endif
	do {

		cpd_mc();
		intr_check();

	} while ( flag[NotZero] && flag[ParOvr] );
}

inline void
z80_cpu::ina(z80_8bit n)
{
#ifdef TRACE
	trace << "IN" << tab << reg.name(A) << lparen << int(n) << rparen;
#endif
	reg.r8(A) = z80io[(reg.r8(A) << bpb) + n];
}

inline void
z80_cpu::in(reg8bit r)
{
#ifdef TRACE
	trace << "IN" << tab << reg.name(r) << lparen << reg.name(C) << rparen;
#endif
	reg.r8(r) = z80io[reg.r16(BC)];
	// Need to diddle flags here.
}

inline void
z80_cpu::in_cp(reg8bit r)
{
#ifdef TRACE
	trace << "INCP" << tab << lparen << reg.name(r) << rparen;
#endif
	reg.r8(r) = z80io[reg.r16(BC)];
	// Need to diddle flags here.
}

inline void
z80_cpu::ini_mc()
{
	z80mem[reg.r16(HL)++] = z80_8bit(z80io[reg.r16(BC)]);
	reg.r8(B) = alu8.dec(reg.r8(B));
}

inline void
z80_cpu::ini()
{
#ifdef TRACE
	trace << "INI";
#endif
	ini_mc();
}

inline void
z80_cpu::inir()
{
#ifdef TRACE
	trace << "INIR";
#endif
	do {

		ini_mc();
		intr_check();

	} while ( flag[NotZero] );
}

inline void
z80_cpu::ind_mc()
{
	z80mem[reg.r16(HL)--] = z80_8bit(z80io[reg.r16(BC)]);
	reg.r8(B) = alu8.dec(reg.r8(B));
}

inline void
z80_cpu::ind()
{
#ifdef TRACE
	trace << "IND";
#endif
	ind_mc();
}

inline void
z80_cpu::indr()
{
#ifdef TRACE
	trace << "INDR";
#endif
	do {

		ind_mc();
		intr_check();

	} while ( flag[NotZero] );
}


inline void
z80_cpu::outa(z80_8bit n)
{
#ifdef TRACE
	trace << "OUT" << tab << lparen << int(n) << rparen
		<< comma << reg.name(A);
#endif
	z80io[(reg.r8(A) << bpb) + n] = reg.r8(A);
}

inline void
z80_cpu::out(reg8bit r)
{
#ifdef TRACE
	trace << "OUT" << tab << lparen << reg.name(C) << rparen
		<< comma << reg.name(r);
#endif
	z80io[reg.r16(BC)] = reg.r8(r);
}


inline void
z80_cpu::oti_mc()
{
	/*
	 * Do DEC first.
	 * See ZSIM by weberj@dialog.informatik.uni-stuttgart.de
	 */
	reg.r8(B) = alu8.dec(reg.r8(B));
	z80io[reg.r16(BC)] = z80mem[reg.r16(HL)++];
}

inline void
z80_cpu::outi()
{
#ifdef TRACE
	trace << "OUTI";
#endif
	oti_mc();
}

inline void
z80_cpu::otir()
{
#ifdef TRACE
	trace << "OTIR";
#endif
	do {

		oti_mc();
		intr_check();

	} while ( flag[NotZero] );
}

inline void
z80_cpu::otd_mc()
{
	reg.r8(B) = alu8.dec(reg.r8(B));
	z80io[reg.r16(BC)] = z80mem[reg.r16(HL)--];
}

inline void
z80_cpu::outd()
{
#ifdef TRACE
	trace << "OUTD";
#endif
	otd_mc();
}

inline void
z80_cpu::otdr()
{
#ifdef TRACE
	trace << "OTDR";
#endif
	do {

		otd_mc();
		intr_check();

	} while ( flag[NotZero] );
}

inline void
z80_cpu::inc(reg8bit r)
{
#ifdef TRACE
	trace << "INC" << tab << reg.name(r);
#endif
	reg.r8(r) = alu8.inc(reg.r8(r));
}

inline void
z80_cpu::inc(reg16bit r)
{
#ifdef TRACE
	trace << "INC" << tab << reg.name(r);
#endif
	reg.r16(r) = alu16.inc(reg.r16(r));
}

inline void
z80_cpu::inc_ind(reg16bit r, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "INC" << tab;
	show_ind(trace, r, dis);
#endif
	z80_16bit loc = reg.r16(r) + dis;
	z80mem[loc] = alu8.inc(z80mem[loc]);
}

inline void
z80_cpu::dec(reg8bit r)
{
#ifdef TRACE
	trace << "DEC" << tab << reg.name(r);
#endif
	reg.r8(r) = alu8.dec(reg.r8(r));
}

inline void
z80_cpu::dec(reg16bit r)
{
#ifdef TRACE
	trace << "DEC" << tab << reg.name(r);
#endif
	reg.r16(r) = alu16.dec(reg.r16(r));
}

inline void
z80_cpu::dec_ind(reg16bit r, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "DEC" << tab;
	show_ind(trace, r, dis);
#endif
	z80_16bit loc = reg.r16(r) + dis;
	z80mem[loc] = alu8.dec(z80mem[loc]);
}

inline void
z80_cpu::neg_acc(void)
{
#ifdef TRACE
	trace << "NEG";
#endif
	reg.r8(A) = alu8.neg(reg.r8(A));
}

inline void
z80_cpu::cpl_acc()
{
#ifdef TRACE
	trace << "CPL";
#endif
	reg.r8(A) = alu8.cpl(reg.r8(A));
}

inline void
z80_cpu::scf()
{
#ifdef TRACE
	trace << "SCF";
#endif
	alu8.scf();
}

inline void
z80_cpu::ccf()
{
#ifdef TRACE
	trace << "CCF";
#endif
	alu8.ccf();
}

inline void
z80_cpu::add_acc(reg8bit r)
{
#ifdef TRACE
	trace << "ADD" << tab << reg.name(A) << comma << reg.name(r);
#endif
	reg.r8(A) = alu8.add(reg.r8(A), reg.r8(r));
}

inline void
z80_cpu::add_acc(z80_8bit n)
{
#ifdef TRACE
	trace << "ADD" << tab << reg.name(A) << comma << hexwordp((int)n);
#endif
	reg.r8(A) = alu8.add(reg.r8(A), n);
}

inline void
z80_cpu::add_acc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "ADD" << tab << reg.name(A) << comma;
	show_ind(trace, ri, dis);
#endif
	reg.r8(A) = alu8.add(reg.r8(A), z80mem[reg.r16(ri) + dis]);
}

inline void
z80_cpu::sub_acc(reg8bit r)
{
#ifdef TRACE
	trace << "SUB" << tab << reg.name(A) << comma << reg.name(r);
#endif
	reg.r8(A) = alu8.sub(reg.r8(A), reg.r8(r));
}

inline void
z80_cpu::sub_acc(z80_8bit n)
{
#ifdef TRACE
	trace << "SUB" << tab << reg.name(A) << comma << hexwordp((int)n);
#endif
	reg.r8(A) = alu8.sub(reg.r8(A), n);
}

inline void
z80_cpu::sub_acc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "SUB" << tab << reg.name(A) << comma;
	show_ind(trace, ri, dis);
#endif
	reg.r8(A) = alu8.sub(reg.r8(A), z80mem[reg.r16(ri) + dis]);
}

inline void
z80_cpu::adc_acc(reg8bit r)
{
#ifdef TRACE
	trace << "ADC" << tab << reg.name(A) << comma << reg.name(r);
#endif
	reg.r8(A) = alu8.adc(reg.r8(A), reg.r8(r));
}

inline void
z80_cpu::adc_acc(z80_8bit n)
{
#ifdef TRACE
	trace << "ADC" << tab << reg.name(A) << comma << hexwordp((int)n);
#endif
	reg.r8(A) = alu8.adc(reg.r8(A), n);
}

inline void
z80_cpu::adc_acc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "ADC" << tab << reg.name(A) << comma;
	show_ind(trace, ri, dis);
#endif
	reg.r8(A) = alu8.adc(reg.r8(A), z80mem[reg.r16(ri) + dis]);
}

inline void
z80_cpu::sbc_acc(reg8bit r)
{
#ifdef TRACE
	trace << "SBC" << tab << reg.name(A) << comma << reg.name(r);
#endif
	reg.r8(A) = alu8.sbc(reg.r8(A), reg.r8(r));
}

inline void
z80_cpu::sbc_acc(z80_8bit n)
{
#ifdef TRACE
	trace << "SBC" << tab << reg.name(A) << comma << hexwordp((int)n);
#endif
	reg.r8(A) = alu8.sbc(reg.r8(A), n);
}

inline void
z80_cpu::sbc_acc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "SBC" << tab
		<< reg.name(A) << comma;
	show_ind(trace, ri, dis);
#endif
	reg.r8(A) = alu8.sbc(reg.r8(A), z80mem[reg.r16(ri) + dis]);
}

inline void
z80_cpu::cp_acc(reg8bit r)
{
#ifdef TRACE
	trace << "CP" << tab << reg.name(r);
#endif
	alu8.cp(reg.r8(A), reg.r8(r));
}

inline void
z80_cpu::cp_acc(z80_8bit n)
{
#ifdef TRACE
	trace << "CP" << tab << hexbyte((int)n);
#endif
	alu8.cp(reg.r8(A), n);
}

inline void
z80_cpu::cp_acc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "CP" << tab;
	show_ind(trace, ri, dis);
#endif
	alu8.cp(reg.r8(A), z80mem[reg.r16(ri) + dis]);
}

inline void
z80_cpu::and_acc(reg8bit r)
{
#ifdef TRACE
	trace << "AND" << tab << reg.name(r);
#endif
	reg.r8(A) = alu8.andop(reg.r8(A), reg.r8(r));
}

inline void
z80_cpu::and_acc(z80_8bit n)
{
#ifdef TRACE
	trace << "AND" << tab << hexbyte((int)n);
#endif
	reg.r8(A) = alu8.andop(reg.r8(A), n);
}

inline void
z80_cpu::and_acc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "AND" << tab;
	show_ind(trace, ri, dis);
#endif
	reg.r8(A) = alu8.andop(reg.r8(A), z80mem[reg.r16(ri) + dis]);
}

inline void
z80_cpu::or_acc(reg8bit r)
{
#ifdef TRACE
	trace << "OR" << tab << reg.name(r);
#endif
	reg.r8(A) = alu8.orop(reg.r8(A), reg.r8(r));
}

inline void
z80_cpu::or_acc(z80_8bit n)
{
#ifdef TRACE
	trace << "OR" << tab << hexbyte((int)n);
#endif
	reg.r8(A) = alu8.orop(reg.r8(A), n);
}

inline void
z80_cpu::or_acc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "OR" << tab;
	show_ind(trace, ri, dis);
#endif
	reg.r8(A) = alu8.orop(reg.r8(A), z80mem[reg.r16(ri) + dis]);
}

inline void
z80_cpu::xor_acc(reg8bit r)
{
#ifdef TRACE
	trace << "XOR" << tab << reg.name(r);
#endif
	reg.r8(A) = alu8.xorop(reg.r8(A), reg.r8(r));
}

inline void
z80_cpu::xor_acc(z80_8bit n)
{
#ifdef TRACE
	trace << "XOR" << tab << hexbyte((int)n);
#endif
	reg.r8(A) = alu8.xorop(reg.r8(A), n);
}

inline void
z80_cpu::xor_acc_ind(reg16bit ri, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "XOR" << tab;
	show_ind(trace, ri, dis);
#endif
	reg.r8(A) = alu8.xorop(reg.r8(A), z80mem[reg.r16(ri) + dis]);
}

inline void
z80_cpu::daa()
{
#ifdef TRACE
	trace << "DAA";
#endif
	alu8.daa(reg.r8(A));
}

inline void
z80_cpu::add_HL(reg16bit r)
{
#ifdef TRACE
	trace << "ADD" << tab << "HL" << reg.name(r);
#endif
	reg.r16(HL) = alu16.add(reg.r16(HL), reg.r16(r));
}

inline void
z80_cpu::add_IX(reg16bit r)
{
#ifdef TRACE
	trace << "ADD" << tab << "IX" << reg.name(r);
#endif
	reg.r16(IX) = alu16.add(reg.r16(IX), reg.r16(r));
}

inline void
z80_cpu::add_IY(reg16bit r)
{
#ifdef TRACE
	trace << "ADD" << tab << "IY" << reg.name(r);
#endif
	reg.r16(IY) = alu16.add(reg.r16(IY), reg.r16(r));
}

inline void
z80_cpu::adc_HL(reg16bit r)
{
#ifdef TRACE
	trace << "ADC"<< tab << "HL" << reg.name(r);
#endif
	reg.r16(HL) = alu16.adc(reg.r16(HL), reg.r16(r));
}

inline void
z80_cpu::sbc_HL(reg16bit r)
{
#ifdef TRACE
	trace << "SBC" << tab << "HL" << reg.name(r);
#endif
	reg.r16(HL) = alu16.sbc(reg.r16(HL), reg.r16(r));
}

inline void
z80_cpu::call(z80_16bit loc)
{
#ifdef TRACE
	trace << "CALL" << tab << hexword((int)loc);
#endif
	_push(reg.r16(PC));
	reg.r16(PC) = loc;
}

inline void
z80_cpu::call(Flags f, z80_16bit loc)
{
#ifdef TRACE
	trace << "CALL" << tab << flag.shortname(f)
		<< comma << hexword((int)loc);
#endif
	if ( flag[f] ) {
		_push(reg.r16(PC));
		reg.r16(PC) = loc;
	}
}

inline void
z80_cpu::call(NotFlags f, z80_16bit loc)
{
#ifdef TRACE
	trace << "CALL" << tab << flag.shortname(f)
		<< comma << hexword((int)loc);
#endif
	if ( flag[f] ) {
		_push(reg.r16(PC));
		reg.r16(PC) = loc;
	}
}

inline void
z80_cpu::ret()
{
#ifdef TRACE
	trace << "RET";
#endif
	reg.r16(PC) = pop();
}

inline void
z80_cpu::ret(Flags f)
{
#ifdef TRACE
	trace << "RET" << tab << flag.shortname(f);
#endif
	if ( flag[f] ) {
		reg.r16(PC) = pop();
	}
}

inline void
z80_cpu::ret(NotFlags f)
{
#ifdef TRACE
	trace << "RET" << tab << flag.shortname(f);
#endif
	if ( flag[f] ) {
		reg.r16(PC) = pop();
	}
}

inline void
z80_cpu::retn()
{
#ifdef TRACE
	trace << "RETN";
#endif
	reg.r16(PC) = pop();
	intr_state( IFF2 );
}

inline void
z80_cpu::reti()
{
#ifdef TRACE
	trace << "RETI";
#endif
	reg.r16(PC) = pop();
	// Anything more???
}

inline void
z80_cpu::jp(z80_16bit loc)
{
#ifdef TRACE
	trace << "JP" << tab << hexword((int)loc);
#endif
	reg.r16(PC) = loc;
}

inline void
z80_cpu::jp(Flags f, z80_16bit loc)
{
#ifdef TRACE
	trace << "JP" << tab << flag.shortname(f) << comma
		<< hexwordp((int)loc);
#endif
	if ( flag[f] ) reg.r16(PC) = loc;
}

inline void
z80_cpu::jp(NotFlags f, z80_16bit loc)
{
#ifdef TRACE
	trace << "JP" << tab << flag.shortname(f) << comma
		<< hexwordp((int)loc);
#endif
	if ( flag[f] ) reg.r16(PC) = loc;
}

inline void
z80_cpu::jp(reg16bit r)
{
#ifdef TRACE
	trace << "JP" << tab;
	show_ind(trace, r);
#endif
	reg.r16(PC) = reg.r16(r);
}

inline void
z80_cpu::jr(z80_8bit_s dis)
{
#ifdef TRACE
	trace << "JR" << tab << showsign(dis)
		<< "  " << hexwordb(reg.r16(PC) + dis);
#endif
	reg.r16(PC) += dis;
}

inline void
z80_cpu::jr(Flags f, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "JR" << tab << flag.shortname(f) 
		<< comma << showsign(dis)
		<< "  " << hexwordb(reg.r16(PC) + dis);
#endif
	if ( flag[f] ) reg.r16(PC) += dis;
}

inline void
z80_cpu::jr(NotFlags f, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "JR" << tab << flag.shortname(f)
		<< comma << showsign(dis)
		<< "  " << hexwordb(reg.r16(PC) + dis);
#endif
	if ( flag[f] ) reg.r16(PC) += dis;
}

inline void
z80_cpu::djnz(reg8bit r, z80_8bit_s dis)
{
#ifdef TRACE
	trace << "DJNZ" << tab << showsign(dis)
		<< "  " << hexwordb(reg.r16(PC) + dis);
#endif
	if ( --reg.r8(r) ) reg.r16(PC) += dis;
}

inline void
z80_cpu::rst(z80_16bit addr)
{
#ifdef TRACE
	trace << "RST" << tab << hexword((int)addr);
#endif
	_push(PC);
	reg.r16(PC) = addr;
}

inline void
z80_cpu::halt()
{
#ifdef TRACE
	trace << "HALT";
#endif
	Halt = 1;
	reg.r16(PC)--;
}

inline void
z80_cpu::ex(reg16bit r1, reg16bit r2)
{
#ifdef TRACE
	trace << "EX" << tab << reg.name(r1) << comma << reg.name(r2);
#endif
	ex_regs16(r1,r2);
}

inline void
z80_cpu::exx()
{
#ifdef TRACE
	trace << "EXX";
#endif
	ex_regs16(BC,BC_Pr);
	ex_regs16(DE,DE_Pr);
	ex_regs16(HL,HL_Pr);
}

inline void
z80_cpu::ei()
{
#ifdef TRACE
	trace << "EI";
#endif
	intr_state( Enabled );
}

inline void
z80_cpu::di()
{
#ifdef TRACE
	trace << "DI";
#endif
	intr_state( Disabled );
}

#ifdef TRACE
inline void
z80_cpu::im(int mode)
#else
inline void
z80_cpu::im(int /*mode*/)
#endif
{
#ifdef TRACE
	trace << "IM" << tab << mode;
#endif
	// Do something someday.
}

#endif
