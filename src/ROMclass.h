#ifndef ROM_class_h
#define ROM_class_h

#include <new>
#include "MemBUS.h"
#include "DataClass.h"

class ROM : public MemBUS {

	MEM_8bit		*load(unsigned int, Data&);
	MEM_8bit		*load(unsigned int);

protected:
	const unsigned int	 size;
	MEM_8bit* const		space;

public:
	//ROM(unsigned int, Data &);
	ROM(unsigned int, const Data &);
	ROM(unsigned int);
	virtual ~ROM();

	MEM_Ref virtual operator[]( address a ) { return( MEM_Ref(*this,a) ); }

	virtual MEM_8bit Value(address a) { return(space[a]); }

	virtual void Assign(address, MEM_8bit) { }
	virtual void AddAssign(address, MEM_8bit) { }
	virtual void SubAssign(address, MEM_8bit) { }

}; // class ROM

#endif
