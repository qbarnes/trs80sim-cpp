#ifndef Z80_alu8_h
#define Z80_alu8_h

#include "Z80.h"
#include "Z80_flags.h"

class z80_alu8 {

	static z80_8bit add_sign_carry_overflow_table[];
	static z80_8bit sub_sign_carry_overflow_table[];
	static z80_8bit add_half_carry_table[];
	static z80_8bit sub_half_carry_table[];
	static char parity_table[256];

	z80_8bit&	zFlags;
	z80_flags	bFlags;

	z80_8bit adder(z80_8bit, z80_8bit);
	z80_8bit suber(z80_8bit, z80_8bit);

	inline	int  parity(z80_8bit);
	inline	void log_flgs(z80_8bit, int);
	inline	void rot_flgs(z80_8bit, int);

public:

	z80_alu8(z80_8bit&);

	z80_8bit inc(z80_8bit);
	z80_8bit dec(z80_8bit);

	z80_8bit add(z80_8bit, z80_8bit);
	z80_8bit adc(z80_8bit, z80_8bit);

	z80_8bit sub(z80_8bit, z80_8bit);
	z80_8bit sbc(z80_8bit, z80_8bit);

	inline	void     cp (z80_8bit, z80_8bit);
	inline	z80_8bit neg(z80_8bit);

	inline  z80_8bit cpl(z80_8bit);
	inline  void     scf();
	inline  void     ccf();

	inline	z80_8bit andop(z80_8bit, z80_8bit);
	inline	z80_8bit orop (z80_8bit, z80_8bit);
	inline	z80_8bit xorop(z80_8bit, z80_8bit);

	z80_8bit rlca(z80_8bit);
	z80_8bit rlc(z80_8bit);

	z80_8bit rla(z80_8bit);
	z80_8bit rl(z80_8bit);

	z80_8bit rrca(z80_8bit);
	z80_8bit rrc(z80_8bit);

	z80_8bit rra(z80_8bit);
	z80_8bit rr(z80_8bit);

	z80_8bit sla(z80_8bit);
	z80_8bit sra(z80_8bit);
	z80_8bit srl(z80_8bit);

	reg8bit_pair_t rld(z80_8bit, z80_8bit);
	reg8bit_pair_t rrd(z80_8bit, z80_8bit);

	inline	void     bit(z80_8bit, z80_8bit);
	inline	z80_8bit set(z80_8bit, z80_8bit);
	inline	z80_8bit res(z80_8bit, z80_8bit);

	z80_8bit daa(z80_8bit);

};  // z80_alu8


inline int
z80_alu8::parity(z80_8bit v)
{
	return(parity_table[v]);
}

inline void
z80_alu8::log_flgs(z80_8bit v, int halfcarry)
{
	z80_flags	regf(zFlags);
	z80_8bit	flg = 0;

	if (parity(v))
		flg |= ParOvrMask;

	if (v == (z80_8bit)0x00)
		flg |= ZeroMask;

	if (v & (z80_8bit)0x80)
		flg |= SignMask;

	if (halfcarry)
		flg |= HalfCarryMask;

	regf.Clear(CarryMask | SubtractMask | ParOvrMask | HalfCarryMask | 
		ZeroMask | SignMask);

	regf.Set(flg);
}

inline	void
z80_alu8::rot_flgs(z80_8bit v, int carry)
{
	z80_flags	regf(zFlags);
	z80_8bit	flg = 0;

	if (parity(v))
		flg |= ParOvrMask;

	if (v == (z80_8bit)0x00)
		flg |= ZeroMask;

	if (v & (z80_8bit)0x80)
		flg |= SignMask;

	if (carry)
		flg |= CarryMask;

	regf.Clear(CarryMask | SubtractMask | ParOvrMask | HalfCarryMask | 
		ZeroMask | SignMask);

	regf.Set(flg);
}

inline void
z80_alu8::cp(z80_8bit v1, z80_8bit v2)
{
	(void)sub(v1, v2);
}

inline z80_8bit
z80_alu8::neg(z80_8bit v1)
{
	return( sub(0, v1) );
}


inline  z80_8bit
z80_alu8::cpl(z80_8bit v)
{
	z80_flags(zFlags).Set(SubtractMask|HalfCarryMask);
	return( v ^= (z80_8bit)0xff );
}

inline  void
z80_alu8::scf()
{
	z80_flags	regf(zFlags);

	regf.Clear(SubtractMask|HalfCarryMask);
	regf.Set(CarryMask);
}

inline  void
z80_alu8::ccf()
{
	z80_flags	regf(zFlags);

	regf.Clear(SubtractMask);
	regf.Xor(CarryMask);
}

inline z80_8bit
z80_alu8::andop(z80_8bit v1, z80_8bit v2)
{
	log_flgs(v1 &= v2, 1);

	return( v1 );
}

inline z80_8bit
z80_alu8::orop(z80_8bit v1, z80_8bit v2)
{
	log_flgs(v1 |= v2, 0);

	return( v1 );
}

inline z80_8bit
z80_alu8::xorop(z80_8bit v1, z80_8bit v2)
{
	log_flgs(v1 ^= v2, 0);

	return( v1 );
}

inline void
z80_alu8::bit(z80_8bit s, z80_8bit v)
{
	z80_flags	regf(zFlags);

	v &= (0x01 << s);

	regf.Clear(SubtractMask);
	regf.Set(HalfCarryMask);
	regf.Set(ZeroMask,(v == 0));
}

inline z80_8bit
z80_alu8::set(z80_8bit s, z80_8bit v)
{
	return(v |= (0x01 << s));
}

inline z80_8bit
z80_alu8::res(z80_8bit s, z80_8bit v)
{
	return(v &= ~(0x01 << s));
}

#endif
