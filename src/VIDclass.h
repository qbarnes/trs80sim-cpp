#ifndef VID_class_h
#define VID_class_h

#include "BUS.h"
#include "keys.h"
#include "HOSTclass.h"
#include "RAMclass.h"

// Spaces print as "'"s with LCMOD on.  Mask wrong?
#ifndef LCMOD
#define	LCMASK	0x00
#else
#define	LCMASK	0x40
#endif

class VID : public RAM {

	void WriteUpdate(address, MEM_8bit);

public:
	VID(unsigned int size) : RAM(size) {}

	virtual MEM_8bit Value(address a) { return(space[a]); }

	virtual inline void Assign(address, MEM_8bit);
	virtual inline void AddAssign(address, MEM_8bit);
	virtual inline void SubAssign(address, MEM_8bit);

};  // class VID

inline void
VID::Assign(address a, MEM_8bit x)
{
	x |= LCMASK;
	space[a] = x;
	WriteUpdate(a,x);
}

inline void
VID::AddAssign(address a, MEM_8bit x)
{
	x |= LCMASK;
	space[a] += x;
	WriteUpdate(a,x);
}

inline void
VID::SubAssign(address a, MEM_8bit x)
{
	x |= LCMASK;
	space[a] -= x;
	WriteUpdate(a,x);
}
#endif
