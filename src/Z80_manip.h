#ifndef Z80_manip_h
#define Z80_manip_h

#include "manip.h"

#if 0
class showsign : public one_arg_manip<showsign,int> {
public:
	showsign(int i) : one_arg_manip<showsign,int>(*this,i) {}
};
#endif

class showsign : public manipBase<showsign> {
public:
	showsign(int i) : manipBase<showsign>(*this), n(i) {}

	template <class Ostream>
	Ostream&
	fct(Ostream &os) const
	{
		std::ios_base::fmtflags f = os.flags(std::ios::showpos);

		os << n;
		os.flags(f);

		return(os);
	}

private:
	const int n;
};

class hexbyte : public manipBase<hexbyte> {
public:
	hexbyte(int i) : manipBase<hexbyte>(*this), n(i) {}

	template <class Ostream>
	Ostream&
	fct(Ostream &os) const
	{
		std::ios_base::fmtflags f = os.flags(std::ios::hex);

		os << std::setw(2) << std::setfill('0') << n;
		os.flags(f);

		return(os);
	}

private:
	const int n;
};

class hexword : public manipBase<hexword> {
public:
	hexword(int i) : manipBase<hexword>(*this), n(i) {}

	template <class Ostream>
	Ostream&
	fct(Ostream &os) const
	{
		std::ios_base::fmtflags f = os.flags(std::ios::hex);

		os << std::setw(4) << std::setfill('0') << n;
		os.flags(f);

		return(os);
	}

private:
	const int n;
};

class hexbytep : public manipBase<hexbytep> {
public:
	hexbytep(int i) : manipBase<hexbytep>(*this), n(i) {}

	template <class Ostream>
	Ostream&
	fct(Ostream &os) const
	{
		std::ios_base::fmtflags f = os.flags(std::ios::hex);

		os << '(' << hexbyte(n) << ')';
		os.flags(f);

		return(os);
	}


private:
	const int n;
};

class hexwordp : public manipBase<hexwordp> {
public:
	hexwordp(int i) : manipBase<hexwordp>(*this), n(i) {}

	template <class Ostream>
	Ostream&
	fct(Ostream &os) const
	{
		std::ios_base::fmtflags f = os.flags(std::ios::hex);

		os << '(' << hexword(n) << ')';
		os.flags(f);

		return(os);
	}


private:
	const int n;
};

class hexbyteb : public manipBase<hexbyteb> {
public:
	hexbyteb(int i) : manipBase<hexbyteb>(*this), n(i) {}

	template <class Ostream>
	Ostream&
	fct(Ostream &os) const
	{
		std::ios_base::fmtflags f = os.flags(std::ios::hex);

		os << '[' << hexbyte(n) << ']';
		os.flags(f);

		return(os);
	}


private:
	const int n;
};

class hexwordb : public manipBase<hexwordb> {
public:
	hexwordb(int i) : manipBase<hexwordb>(*this), n(i) {}

	template <class Ostream>
	Ostream&
	fct(Ostream &os) const
	{
		std::ios_base::fmtflags f = os.flags(std::ios::hex);

		os << '[' << hexword(n) << ']';
		os.flags(f);

		return(os);
	}

private:
	const int n;
};

#endif
