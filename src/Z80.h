#ifndef	Z80_h
#define	Z80_h

typedef unsigned char	z80_8bit;
typedef	unsigned short	z80_16bit;

typedef char	z80_8bit_s;
typedef	short	z80_16bit_s;

typedef	unsigned int	Bit;
typedef	unsigned int	Flag;

const	unsigned int	bpb = 8;	// bits per byte
#define	MSB(x)		((z80_8bit)((x) >> bpb))
#define	LSB(x)		((z80_8bit)(x))

enum reg8bit {
	A,
	F,
	B,
	C,
	D,
	E,
	H,
	L,
	A_Pr,
	F_Pr,
	B_Pr,
	C_Pr,
	D_Pr,
	E_Pr,
	H_Pr,
	L_pr,
	I,
	R,
	IX_hi,
	IX_lo,
	IY_hi,
	IY_lo,
	SP_hi,
	SP_lo,
	PC_hi,
	PC_lo,
	reg8FIRST = A,
	reg8LAST = PC_lo
};

enum reg16bit {
	AF,
	BC,
	DE,
	HL,
	AF_Pr,
	BC_Pr,
	DE_Pr,
	HL_Pr,
	IR,
	IX,
	IY,
	SP,
	PC,
	reg16FIRST = AF,
	reg16LAST = PC
};

enum Flags {
	Carry     = 0,
	Subtract  = 1,
	ParOvr    = 2,
	Ind1      = 3,
	HalfCarry = 4,
	Ind2      = 5,
	Zero      = 6,
	Sign      = 7
};

enum NotFlags {
	NotCarry     = Carry,
	NotSubtract  = Subtract,
	NotParOvr    = ParOvr,
	NotInd1      = Ind1,
	NotHalfCarry = HalfCarry,
	NotInd2      = Ind2,
	NotZero      = Zero,
	NotSign      = Sign
};

enum FlagsMask {
	CarryMask     = 1 << Carry,
	SubtractMask  = 1 << Subtract,
	ParOvrMask    = 1 << ParOvr,
	Ind1Mask      = 1 << Ind1,
	HalfCarryMask = 1 << HalfCarry,
	Ind2Mask      = 1 << Ind2,
	ZeroMask      = 1 << Zero,
	SignMask      = 1 << Sign,
	AllMask       = 0xff,
	NilMask       = 0x00
};

typedef struct {

	z80_8bit	r1, r2;

} reg8bit_pair_t;

#endif
