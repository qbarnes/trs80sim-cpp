#ifndef IO_BUS_h
#define IO_BUS_h

#include "BUS.h"

typedef	BUS_8bit	IO_8bit;

class IO_Ref;

class IO_BUS
{
	friend class IO_Ref;

public:
	IO_Ref virtual operator[](IO_8bit a) = 0;

	virtual IO_8bit Value(IO_8bit) { return 0; };
	virtual void Assign(IO_8bit, IO_8bit) { };
	virtual void AddAssign(IO_8bit, IO_8bit) { };
	virtual void SubAssign(IO_8bit, IO_8bit) { };

};  // class IO_BUS

class IO_Ref {
private:
	class IO_BUS& io;
	IO_8bit addr;

public:
	IO_Ref(IO_BUS& m, IO_8bit a) : io(m), addr(a) { }

	// This constructor is to shut up the GNU compiler's warnings.
//	IO_Ref(const IO_Ref& c) : io(c.io), addr(c.addr) { }

	inline operator IO_8bit();
	inline operator int();
	inline IO_Ref& operator=(IO_8bit);
	inline IO_Ref& operator+=(IO_8bit);
	inline IO_Ref& operator-=(IO_8bit);

}; // class IO_Ref


inline IO_Ref::operator int()
{
    return io.Value(addr);
}

inline IO_Ref::operator IO_8bit()
{
    return io.Value(addr);
}

inline IO_Ref& IO_Ref::operator=(IO_8bit x)
{
    io.Assign(addr, x);
    return *this;
}

inline IO_Ref& IO_Ref::operator+=(IO_8bit x)
{
    io.AddAssign(addr, x);
    return *this;
}

inline IO_Ref& IO_Ref::operator-=(IO_8bit x)
{
    io.AddAssign(addr, x);
    return *this;
}

#endif
