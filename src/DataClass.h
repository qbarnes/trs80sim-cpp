#ifndef DataClass_h
#define DataClass_h

// #include	<File.h>
#include	<ios>
#include	<fstream>
#include	<iosfwd>

class Data
{

public:
	inline virtual int	get(unsigned char&) = 0;

};  // Data


class DataFile : public Data
{
	std::ifstream	file;

public:
	DataFile(char *);
	virtual ~DataFile() {}

	virtual int	get(unsigned char&);
};

inline DataFile::DataFile(char *f) : file(f,std::ios::in)
{}

inline int
DataFile::get(unsigned char& c)
{
	c = file.get();
	return(file.fail() == 0);
}

class DataMem : public Data
{
	const unsigned char		*mem;
	const unsigned char		*end;
	unsigned char 			*loc;

public:
	inline DataMem(unsigned char *, unsigned long);

	inline virtual int	get(unsigned char&);
};

inline DataMem::DataMem(unsigned char *m, unsigned long l) : 
	mem(m), end(m + l), loc(m)
	{}

inline int
DataMem::get(unsigned char& c)
{
	if ( loc <= end ) {
		c = *loc++;
		return 1;
	} else {
		return 0;
	}
}

#endif
