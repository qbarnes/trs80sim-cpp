#ifndef Z80_fmt
#define Z80_fmt

#include <ios>
#include <ostream>
#include <iomanip>

class showsign { };

class hexbyte { };
class hexword { };

class hexbytep { };
class hexwordp { };

class hexbyteb { };
class hexwordb { };

std::ostream&
operator<<(std::ostream& os, showsign n);

std::ostream&
operator<<(std::ostream& os, hexbyte n);

std::ostream&
operator<<(std::ostream& os, hexword n);

std::ostream&
operator<<(std::ostream& os, hexbytep n);

std::ostream&
operator<<(std::ostream& os, hexwordp n);

std::ostream&
operator<<(std::ostream& os, hexbyteb n);

std::ostream&
operator<<(std::ostream& os, hexwordb n);

#endif
