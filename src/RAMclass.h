#ifndef RAM_class_h
#define RAM_class_h

#include <new>
#include "MemBUS.h"

class RAM : public MemBUS {

protected:
	const unsigned int	 size;
	MEM_8bit* const		space;

public:
	RAM(unsigned int u_size) : size(u_size), space(new MEM_8bit[u_size]) {}
	virtual ~RAM() { delete space; }

	MEM_Ref virtual operator[]( address a ) { return( MEM_Ref(*this,a) ); }

	virtual MEM_8bit Value(address a) { return(space[a]); }

	virtual void Assign(address a, MEM_8bit x) { space[a] = x; }
	virtual void AddAssign(address a, MEM_8bit x) { space[a] += x; }
	virtual void SubAssign(address a, MEM_8bit x) { space[a] -= x; }

};  // class RAM

#endif
