#ifndef Z80_alu16_h
#define Z80_alu16_h

#include "Z80.h"
#include "Z80_flags.h"

/*
 * alu code borrowed from Nick Sayer's Z-80 emulator code.
 * Copyright 1985, Freely distributable.
 */

class z80_alu16 {

	z80_8bit&	zFlags;
	z80_flags	bFlags;

	z80_8bit	adder(z80_16bit&, z80_16bit, z80_8bit);

public:

	z80_alu16(z80_8bit&);

	inline	z80_16bit inc(z80_16bit);
	inline	z80_16bit dec(z80_16bit);

	inline	z80_16bit add(z80_16bit, z80_16bit);

	inline	z80_16bit adc(z80_16bit, z80_16bit);
	inline	z80_16bit sbc(z80_16bit, z80_16bit);

};  // z80_alu16


inline z80_16bit
z80_alu16::inc(z80_16bit v)
{
	// No flags to adjust on 16 bit operation.
	return( ++v );
}

inline z80_16bit
z80_alu16::dec(z80_16bit v)
{
	// No flags to adjust on 16 bit operation.
	return( --v );
}

inline z80_16bit
z80_alu16::adc(z80_16bit v1, z80_16bit v2)
{
	bFlags[Subtract] = 0;
	zFlags = adder(v1, v2 + bFlags[Carry], zFlags);

	return( v1 );
}

inline z80_16bit
z80_alu16::sbc(z80_16bit v1, z80_16bit v2)
{
	bFlags[Subtract] = 1;
	zFlags = adder(v1, -(v2 + bFlags[Carry]), zFlags);

	return( v1 );
}

inline z80_16bit
z80_alu16::add(z80_16bit v1, z80_16bit v2)
{
	unsigned int	sum;

	sum = v1 + v2;
	bFlags[Carry]    = ( sum > 0xffff );
	bFlags[Subtract] = 0;

	return( (z80_16bit)sum );
}

#endif
