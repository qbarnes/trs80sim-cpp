#ifndef X_host_h
#define X_host_h

#include "keys.h"

#include "IVhost.h"

class Host
{
	World		*world;
	TrsHost		*trshost;

public:
	Host(char *, int, char **);

	int KeyEvents( struct key_event * );
	void DisplayEvent( unsigned int, char );

	int Poll();

};  // class Host

#endif
