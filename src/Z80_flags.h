#ifndef Z80_flags_h
#define Z80_flags_h

#include "Z80.h"

class Flag_Ref;
class Not_Flag_Ref;

class z80_flags
{
	friend	class Flag_Ref;
	friend	class Not_Flag_Ref;

	z80_8bit& z80_flag_reg;

	static const char *const short_name[8];
	static const char *const long_name[8];

	static const char *const not_short_name[8];
	static const char *const not_long_name[8];

#if 0
	struct regflgs {
		unsigned int	:24;
		unsigned int	sign:1;
		unsigned int	zero:1;
		unsigned int	ind2:1;
		unsigned int	halfcarry:1;
		unsigned int	ind1:1;
		unsigned int	parovr:1;
		unsigned int	subtract:1;
		unsigned int	carry:1;
	};

	union reg {
	public:
		struct regflgs flgs;
		unsigned int r;

		reg(z80_8bit f) : r((unsigned int)f) {}
		z80_8bit value() { return (r); }
	};

	//reg r(z80_flag_reg);
	//r.flgs.subtract = 1;
	//r.flgs.sign = 1;
	//return(z80_flag_reg = r.value());
#endif
public:
	z80_flags(z80_8bit& f) : z80_flag_reg(f) { }


	inline Flag_Ref operator[](Flags);
	inline Not_Flag_Ref operator[](NotFlags);

	inline int Value(Flags);
	inline int Value(NotFlags);

	inline z80_8bit Clear(z80_8bit = AllMask);
	inline z80_8bit Set(z80_8bit = AllMask);
	inline z80_8bit Set(z80_8bit, z80_8bit);
	inline z80_8bit Xor(z80_8bit);

	inline void Assign(Flags, int);
	inline void Assign(NotFlags, int);

	inline	const char *shortname(Flags);
	inline	const char *shortname(NotFlags);

	inline	const char *longname(Flags);
	inline	const char *longname(NotFlags);

private:
	inline  FlagsMask f_mask(Flags);
	inline  FlagsMask f_mask(NotFlags);

};  // class z80_flags

class Flag_Ref {
private:
	class z80_flags& flags;
	Flags the_flag;

public:
	Flag_Ref(z80_flags& f, Flags flg) : flags(f), the_flag(flg) { }

	// This constructor is to shut up the GNU compiler's warnings.
	// Flag_Ref(const Flag_Ref& c) : flags(c.flags), the_flag(c.the_flag) { }

	inline operator int();
	inline Flag_Ref& operator=(int);
	inline Flag_Ref& operator^=(int);


}; // class Flag_Ref


class Not_Flag_Ref {
private:
	class z80_flags& flags;
	NotFlags the_flag;

public:
	Not_Flag_Ref(z80_flags& f, NotFlags flg) : flags(f), the_flag(flg) { }

	// This constructor is to shut up the GNU compiler's warnings.
	// Not_Flag_Ref(const Not_Flag_Ref& c) : flags(c.flags), the_flag(c.the_flag) { }

	inline operator int();
	inline Not_Flag_Ref& operator=(int);
	inline Not_Flag_Ref& operator^=(int);

}; // class Not_Flag_Ref


inline Flag_Ref
z80_flags::operator[](Flags f)
{
	return( Flag_Ref(*this,f) );
}

inline Not_Flag_Ref
z80_flags::operator[](NotFlags f)
{
	return( Not_Flag_Ref(*this,f) );
}

inline  FlagsMask
z80_flags::f_mask(Flags f)
{
	return( (FlagsMask)(1 << int(f)) );
}

inline  FlagsMask
z80_flags::f_mask(NotFlags f)
{
	return( (FlagsMask)(1 << int(f)) );
}

inline int
z80_flags::Value(Flags f)
{
	return( (z80_flag_reg & f_mask(f)) != 0 );
}

inline int
z80_flags::Value(NotFlags f)
{
	return( (z80_flag_reg & f_mask(f)) == 0 );
}

inline z80_8bit
z80_flags::Clear(z80_8bit fm)
{
	return( z80_flag_reg &= ~fm );
}

inline z80_8bit 
z80_flags::Set(z80_8bit fm)
{
	return( z80_flag_reg |= fm );
}

inline z80_8bit 
z80_flags::Set(z80_8bit fm, z80_8bit v)
{
	if (v) {
		Set(fm);
	} else {
		Clear(fm);
	}
	return(z80_flag_reg);
}

inline z80_8bit 
z80_flags::Xor(z80_8bit fm)
{
	return( z80_flag_reg ^= fm );
}

inline void
z80_flags::Assign(Flags f, int v)
{
	Set(f_mask(f),v);
}

inline void
z80_flags::Assign(NotFlags f, int v)
{
	Set(f_mask(f),!v);
}

inline const char *
z80_flags::shortname(Flags f)
{
	return( short_name[f] );
}

inline const char *
z80_flags::shortname(NotFlags f)
{
	return( not_short_name[f] );
}

inline const char *
z80_flags::longname(Flags f)
{
	return( long_name[f] );
}

inline const char *
z80_flags::longname(NotFlags f)
{
	return( not_long_name[f] );
}

inline
Flag_Ref::operator int()
{
    return( flags.Value(the_flag) );
}

inline
Not_Flag_Ref::operator int()
{
    return( flags.Value(the_flag) );
}

inline
Flag_Ref& Flag_Ref::operator=(int v)
{
    flags.Assign(the_flag, v);
    return *this;
}

inline
Not_Flag_Ref& Not_Flag_Ref::operator=(int v)
{
    flags.Assign(the_flag, v);
    return *this;
}

inline 
Flag_Ref& Flag_Ref::operator^=(int v)
{
	flags.Assign(the_flag, flags.Value(the_flag) ^ v);
	return *this;
}

inline 
Not_Flag_Ref& Not_Flag_Ref::operator^=(int v)
{
	flags.Assign(the_flag, flags.Value(the_flag) ^ v);
	return *this;
}

#endif
