#ifndef Z80_reg_h
#define Z80_reg_h

#include <iostream>
#include "Z80.h"

class z80_registers {

	// The registers.

	union {
		z80_8bit   r8[reg8LAST   - reg8FIRST  + 1];
		z80_16bit  r16[reg16LAST - reg16FIRST + 1];
	} registers;

	static const char *const reg8_name[reg8LAST - reg8FIRST + 1];
	static const char *const reg16_name[reg16LAST - reg16FIRST + 1];

public:
	z80_registers(void);

	inline	z80_8bit&  r8(reg8bit);
	inline	z80_16bit& r16(reg16bit);

	inline	const char *name(reg8bit);
	inline	const char *name(reg16bit);

	inline	z80_8bit& lsb(reg16bit);
	inline	z80_8bit& msb(reg16bit);

	void	dump(std::ostream& = std::cout);

};  // z80_registers


inline	z80_8bit&
z80_registers::r8(reg8bit r)
{
	return registers.r8[r];
}
inline	z80_16bit&
z80_registers::r16(reg16bit r)
{
	return registers.r16[r];
}

inline const char *
z80_registers::name(reg8bit r)
{
	return( reg8_name[r] );
}

inline const char *
z80_registers::name(reg16bit r)
{
	return( reg16_name[r] );
}

#ifdef LITTLE_ENDIAN
inline	z80_8bit&
z80_registers::lsb(reg16bit r)
{
	return( registers.r8[(reg8bit)(r * 2)] );
}

inline	z80_8bit&
z80_registers::msb(reg16bit r)
{
	return( registers.r8[(reg8bit)(r * 2 + 1)] );
}
#else
inline	z80_8bit&
z80_registers::lsb(reg16bit r)
{
	return( registers.r8[(reg8bit)(r * 2 + 1)] );
}

inline	z80_8bit&
z80_registers::msb(reg16bit r)
{
	return( registers.r8[(reg8bit)(r * 2)] );
}
#endif

#endif
