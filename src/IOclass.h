#ifndef IO_class_h
#define IO_class_h

#include <new>
#include "IO_BUS.h"

class IO : public IO_BUS {

protected:
	const unsigned int	 size;
	IO_8bit* const		space;

public:
	IO(unsigned int u_size) : size(u_size), space(new IO_8bit[u_size]) {}
	virtual ~IO() { delete space; }

	IO_Ref virtual operator[]( IO_8bit a ) { return( IO_Ref(*this,a) ); }

	virtual IO_8bit Value(IO_8bit a) { return(space[a]); }

	virtual void Assign(IO_8bit a, IO_8bit x) { space[a] = x; }
	virtual void AddAssign(IO_8bit a, IO_8bit x) { space[a] += x; }
	virtual void SubAssign(IO_8bit a, IO_8bit x) { space[a] -= x; }

};  // class IO

#endif
