#ifndef IVhost_h
#define IVhost_h

#include <InterViews/scene.h>
#include <InterViews/world.h>
#include <InterViews/textdisplay.h>
#include <InterViews/painter.h>
#include <InterViews/canvas.h>
#include <InterViews/interactor.h>
#include <InterViews/sensor.h>
#include <InterViews/shape.h>
#include <InterViews/font.h>

class KeyQueue
{
	unsigned int	size;
	unsigned int	count;
	char		*q;
	unsigned int	head;
	unsigned int	tail;

public:
	KeyQueue(unsigned int s) :
		size(s),
		count(0),
		q(new char[s]),
		head(0),
		tail(0)
		{}
	~KeyQueue() { delete q; }

	void wrap(unsigned int &v)
	{
		if ( v == size ) v = 0;
	}

	int empty()
	{
		return( count == 0 );
	}

	int full()
	{
		return( count == size );
	}

	void enq(char c)
	{
		if ( !full() ) {
			q[tail] = c;
			wrap(++tail);
			count++;
		}
	}

	char deq()
	{
		char	ret = 0;

		if ( !empty() ) {
			ret = q[head];
			wrap(++head);
			count--;
		}

		return( ret );
	}

};  // class KeyQueue


class TrsHost : public Interactor {
	// static const unsigned int cols, rows, size;
	enum { cols = 64, rows = 16, size = cols * rows };

	char screen[rows][cols];

	unsigned int font_height, font_width;
	static class Font* font;

	void initscreen(char);

	KeyQueue	kq;

public:
	TrsHost();

	void Reconfig();
	void Resize();
	virtual void Redraw(Coord, Coord, Coord, Coord);
	void Handle(Event&);

	void Write(unsigned int, char);


	int newkey(char *);
};

#endif
