#ifndef MANIP_H
#define MANIP_H

// Borrowed from article by Klaus Kreft & Angelika Langer in C/C++ Journal.

#include <ostream>
#include <iomanip>

template <class Manip> class manipBase {
public:
	manipBase(const Manip& m) {}

	template <class Stream>
	Stream& manipulate(Stream& str) const
	{
		static_cast<const Manip&>(*this).fct(str);
		return str;
	}
};

template <class Ostream, class Manip>
Ostream&
operator<< (Ostream& os, const manipBase<Manip>& m)
{
	return m.manipulate(os);
}

#if 0
template <class Manip, typename type>
class one_arg_manip : public manipBase <Manip> {
public:
	one_arg_manip(const Manip& m, type a) : 
		manipBase<Manip>(*this), value(a) {}

private:
	const type value;
};
#endif

#endif
