#ifndef MemBUS_h
#define MemBUS_h

#include "BUS.h"

typedef	BUS_8bit	MEM_8bit;

class MEM_Ref;

class MemBUS
{
	friend class MEM_Ref;

public:
	MEM_Ref virtual operator[](address a) = 0;

	virtual MEM_8bit Value(address) { return 0; };
	virtual void Assign(address, MEM_8bit) { };
	virtual void AddAssign(address, MEM_8bit) { };
	virtual void SubAssign(address, MEM_8bit) { };

};  // class MemBUS

class MEM_Ref {
private:
	class MemBUS& mem;
	address addr;

public:
	MEM_Ref(MemBUS& m, address a) : mem(m), addr(a) { }

	// This constructor is to shut up the GNU compiler's warnings.
//	MEM_Ref(const MEM_Ref& c) : mem(c.mem), addr(c.addr) { }

	inline operator MEM_8bit();
	inline operator int();
	inline operator unsigned int();
	inline MEM_Ref& operator=(MEM_8bit);
	inline MEM_Ref& operator+=(MEM_8bit);
	inline MEM_Ref& operator-=(MEM_8bit);

}; // class MEM_Ref


inline MEM_Ref::operator int()
{
    return mem.Value(addr);
}

inline MEM_Ref::operator MEM_8bit()
{
    return mem.Value(addr);
}

inline MEM_Ref::operator unsigned int()
{
    return mem.Value(addr);
}

inline MEM_Ref& MEM_Ref::operator=(MEM_8bit x)
{
    mem.Assign(addr, x);
    return *this;
}

inline MEM_Ref& MEM_Ref::operator+=(MEM_8bit x)
{
    mem.AddAssign(addr, x);
    return *this;
}

inline MEM_Ref& MEM_Ref::operator-=(MEM_8bit x)
{
    mem.AddAssign(addr, x);
    return *this;
}

#endif
