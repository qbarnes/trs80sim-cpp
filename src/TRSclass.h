#ifndef TRS_class_h
#define TRS_class_h

#include "Z80_cpu.h"

#include "IO_BUS.h"
#include "IOclass.h"

#include "DataClass.h"

#include "MemBUS.h"
#include "RAMclass.h"
#include "ROMclass.h"
#include "KBDclass.h"
#include "VIDclass.h"

class trs_memory : public MemBUS {

	ROM		rom;
	RAM		dev;
	KBD		kbd;
	VID		vid;
	RAM		ram;

	static unsigned char inst[10];

public:
//	trs_memory() : rom(0x3000), kbd(0x400), vid(0x400), ram(0xc000) {}
#if 0
	trs_memory() : rom(0x4000, DataMem(inst,sizeof(inst))),
		dev(0x800), kbd(0x400), vid(0x400), ram(0xc000) {}
#else
	trs_memory() : 
		rom(0x3000, DataFile("ROM")),
		dev(0x800),
		kbd(0x400),
		vid(0x400),
		ram(0xc000)
		{}
#endif
	virtual ~trs_memory() {}

	MEM_Ref operator[]( address a ) {

		if ( a >= 0x4000 ) {

			return( ram[a - 0x4000] );

		} else if ( a < 0x3000 ) {

			return( rom[a] );

		} else if ( a >= 0x3c00 ) {

			return( vid[a - 0x3c00] );

		} else if ( a >= 0x3800 ) {

			return( kbd[a - 0x3800] );

		} else {

			return( dev[a - 0x3000] );

		}
	}
};


class trs_io : public IO_BUS {

	IO	io;

public:
	trs_io() : io(0x100) {}

	virtual ~trs_io() {}

	IO_Ref operator[]( IO_8bit a ) { return( io[a] ); }
};

class TRS_80 {

	trs_memory	z80mem;
	trs_io		z80io;
	z80_cpu		z80cpu;

public:
	TRS_80() : z80cpu(z80mem, z80io) {}

	int run();

};

#endif
